1) A working canvas rendering from WebAssembly (60fps)

https://chessforeva.codeberg.page/wasm/canvas_render.htm

The same is in webworker script as fast as can
(files: R_sample.htm, R_worker.js, R.wasm)
 
https://chessforeva.codeberg.page/wasm/R_sample.htm

----------------------------
Originally blog source:
https://compile.fi/canvas-filled-three-ways-js-webassembly-and-webgl/


2) u64chess WebAssembly sample

https://chessforeva.codeberg.page/wasm/u64wasmSample.htm

sources of .c at
https://github.com/Chessforeva/Cpp4chess     u64_chess
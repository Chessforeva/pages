// worker

// WebAssembly objects, will be accessible
var memSize = 0;
var _memory = {};
var _instance = {};
var _canvas = {};
var _data = {};
var _ctx = {};
var _img = {};
var _pointer = {};		// to memory...
var _loaded = 0;

self.onmessage = function(event) {
	
	var s = event.data;
	if( typeof(s.canvas)!="undefined") {
			_canvas = s.canvas;
			start_init();
			return true;
		}
		
	if(s.substr(0,6)=="render" && _loaded==2) {
		var timestamp = parseInt(s.substr(6));
		
		_instance.exports._render(timestamp*90);
		_ctx.putImageData(_img, 0, 0);
		postMessage("ok-rendered");
	}
	
	return true;
	
}

function start_init() {
	
	if(_loaded==0) setTimeout('start_init()',999);			// canvas is ready, but wasm may not be loaded
	else {
		_ctx = _canvas.getContext('2d', {
			alpha: false,
			antialias: false,
			depth: false
			});
		var height = _canvas.height;
		var width = _canvas.width;
		_pointer = _instance.exports._init(width, height);		  
		_data = new Uint8ClampedArray(_memory.buffer, _pointer, width * height * 4);
		_img = new ImageData(_data, width, height);
		_loaded = 2;
		postMessage("canrender");
		}
}

function LoadWasm() {

//---------------------------------------------------------------------------------
// C program to compile with emscripten
//
// emcc full.c -O2 -s WASM=1 -o full.html -s IMPORTED_MEMORY=1 
//
// Notes:
//   1) Tested emcc on Windows 10, emcc up-to-dated 18.jun.2022
//   2) optimization level -O2 is the only that works like a charm
//   3) Imported memory makes accessible data[] area from javascript,
//            that adds   (import "env" "memory" (memory $env.memory 256 256)) to .wasm
//   4) - omitted -
//   5) In case, it's good to use wasm2wat tools or sites to see bugs, or differences from good working samples
//
//  Original site: https://gitlab.com/infocompile/canvas-article
// 

//  full.c
/*

#define PI 3.14159265358979323846
#define RAD 6.283185307179586
#define COEFF_1 0.7853981633974483
#define COEFF_2 2.356194490192345
#define BLADES 3
#define CYCLE_WIDTH 100
#define BLADES_T_CYCLE_WIDTH 300
#include <math.h>
#include <stdlib.h>
#include <emscripten.h>

int height;
int width;
int pixelCount;
int ch;
int cw;
double maxDistance;
//
//We'll cheat a bit and just allocate loads of memory
//so we don't have to implement malloc
//
int data[2000000];

int* EMSCRIPTEN_KEEPALIVE init(int cWidth, int cHeight) {
  width = cWidth;
  height = cHeight;
  pixelCount = width * height;
  ch = height >> 1;
  cw = width >> 1;
  maxDistance = sqrt(ch * ch + cw * cw);
  // data = malloc(pixelCount * sizeof(int));
  return &data[0];
}

double customAtan2(int y, int x) {
	double abs_y = abs(y) + 1e-10;
	double angle;
	if (x >= 0) {
		double r = (x - abs_y) / (x + abs_y);
    angle = 0.1963 * r * r * r - 0.9817 * r + COEFF_1;
	} else {
		double r = (x + abs_y) / (abs_y - x);
    angle = 0.1963 * r * r * r - 0.9817 * r + COEFF_2;
	}
	return y < 0 ? -angle : angle;
}

// Using the 'native' fmod would require us to provide the module with asm2wasm...
double customFmod(double a, double b)
{
  return (a - b * floor(a / b));
}

void EMSCRIPTEN_KEEPALIVE render(double timestamp) {
  int scaledTimestamp = floor(timestamp / 10.0 + 2000.0);
  for (int y = 0; y < height; y++) {
    int dy = ch - y;
    int dysq = dy * dy;
    int yw = y * width;
    for (int x = 0; x < width; x++) {
      int dx = cw - x;
      int dxsq = dx * dx;
      double angle = customAtan2(dx, dy) / RAD;
      // Arbitrary mangle of the distance, just something that looks pleasant
      int asbs = dxsq + dysq;
      double distanceFromCenter = sqrt(asbs);
      double scaledDistance = asbs / 400.0 + distanceFromCenter;
      double lerp = 1.0 - (customFmod(fabs(scaledDistance - scaledTimestamp + angle * BLADES_T_CYCLE_WIDTH), CYCLE_WIDTH)) / CYCLE_WIDTH;
      // Fade R more slowly
      double absoluteDistanceRatioGB = 1.0 - distanceFromCenter / maxDistance;
      double absoluteDistanceRatioR = absoluteDistanceRatioGB * 0.8 + 0.2;
      int fadeB = round(50.0 * lerp * absoluteDistanceRatioGB);
      int fadeR = round(240.0 * lerp * absoluteDistanceRatioR * (1.0 + lerp) / 2.0);
      int fadeG = round(120.0 * lerp * lerp * lerp * absoluteDistanceRatioGB);
      data[yw + x] =
        (255 << 24) |   // A
        (fadeB << 16) | // B
        (fadeG << 8) |  // G
        fadeR;          // R
    }
  }
}
*/

//---------------------------------------------------------------------------------

// And after, as result, it is compiled to R.wasm file,
		
memSize = 256;
_memory = new WebAssembly.Memory({
      initial: memSize,
      maximum: memSize
    });

// Load .wasm
(async () => {
  const response = await fetch('R.wasm');
  const bytes = await response.arrayBuffer();
  const { instance } = await WebAssembly.instantiate(bytes, {
    env: { memory: _memory }
  });

  _instance = instance;
  _loaded = 1;
})();

}

LoadWasm();



/*
// These not used, but can be useful.
//
// To pass other data for requests and answers. The .wasm takes it as a char byte value (0-255).
//


// makes string by looping returned chars "ab...\0"
function getStr() {
	var s="",i=0;
	for(;;) {
		var c=_instance.exports._get(i++);
		if(!c) break;
		s+=String.fromCharCode(c);
	}
	return s;
}

// writes string by looping chars "ab...\0"
function setStr(s) {
	var i=0;
	for(; i<s.length; i++) {
		var c=s.charCodeAt(i);
		_instance.exports._set_data(i,c);
	}
	_instance.exports._set_data(i,0);
}

*/